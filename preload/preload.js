var axios = require("axios");
var httpsProxyAgent = require('https-proxy-agent');
// Full IPC
var ipc = require("electron").ipcRenderer;
// Limited IPC for safety
document.ipc = require("./ipc.js")

document.toughCookie = require("tough-cookie");
document.proxiedHttpRequest = function (options, proxy, cookies, resolve, reject) {
    var agent = undefined;
    if (proxy)
        agent = new httpsProxyAgent(proxy);
    axios(
        extend({
            httpsAgent: agent,
            jar: cookies,
            withCredentials: true,
        }, options)
    ).then(function (res) {
        resolve(res.data);
    }, function (err) {
        console.error(err);
        reject(err.response, err);
    });
}

document.startSteam = function (account) {
    ipc.send("start-steam", account)
}